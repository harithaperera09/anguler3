# stage 1
FROM node:12.2.0
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

# stage 2
# Bundle app source
COPY . .

# start app
CMD ng serve --host 0.0.0.0